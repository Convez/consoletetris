//
// Created by convez on 04/04/2020.
//

#include "TetrisShape.h"

namespace convez::games::consoleTetris
{
    TetrisShape::IndexFunctionVector_t TetrisShape::_rotationIndexCalculatorsVec =
            {
                [](int x, int y, int wdt) { return wdt*y +x; },    // 0   degrees rotation
                [](int x, int y, int wdt) { return 12 +y -wdt*x;}, // 90  degrees rotation
                [](int x, int y, int wdt) { return 15 -wdt*y -x;}, // 180 degrees rotation
                [](int x, int y, int wdt) { return 3 - y + wdt*x;} // 270 degrees rotation
            };

    TetrisShape::TetrisShape(std::string iShapeModel, int iShapeHeight, int iShapeWidth, char iDrawingSymbol, int iDrawingColor, std::pair<int, int> iStartPosition) :
        _shapeModel(iShapeModel),
        _shapeHeight(iShapeHeight),
        _shapeWidth(iShapeWidth),
        _drawingSymbol(iDrawingSymbol),
        _drawingColor(iDrawingColor),
        _shapeRotation(0),
        _currentPosition(std::move(iStartPosition)){
    }

    char TetrisShape::elementAt(int iXPos, int iYPos) const {
        return _shapeModel.at(_rotationIndexCalculatorsVec.at(_shapeRotation)(iXPos, iYPos, 4));
    }

    bool TetrisShape::tryMoveTo(std::pair<int, int> iPotentialNewPosition) {
        if(_windowHandler->doesPieceFit(*this, iPotentialNewPosition))
        {
            _currentPosition = iPotentialNewPosition;
            return true;
        }
        return false;
    }

    bool TetrisShape::tryMoveDown() {
        return tryMoveTo(std::make_pair(_currentPosition.first,_currentPosition.second+1));
    }

    bool TetrisShape::tryMoveLeft() {
        return tryMoveTo(std::make_pair(_currentPosition.first-1,_currentPosition.second));
    }

    bool TetrisShape::tryMoveRight() {
        return tryMoveTo(std::make_pair(_currentPosition.first+1,_currentPosition.second));
    }

    bool TetrisShape::tryRotate() {
        int aOldRotation = _shapeRotation;
        rotate();
        if(!_windowHandler->doesPieceFit(*this, _currentPosition))
        {
            _shapeRotation = aOldRotation;
            return false;
        }
        return true;
    }
}
