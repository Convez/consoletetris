//
// Created by convez on 04/04/2020.
//

#ifndef CONSOLETETRIS_PLAYER_H
#define CONSOLETETRIS_PLAYER_H

#include <cmath>
#include <string>
#include <utility>

namespace convez::games::consoleTetris
{
    class Player {
    public:
        Player(int scoreIncrease, std::string iPlayerName) :_playerName(std::move(iPlayerName)), _isAlive(true), _score(0), _scoreIncrease(scoreIncrease) {}

        // Setters
        void die() {_isAlive = false; }
        // This increases the score of the player.
        // The score is increased if:
        //  - The player is alive after a game tick:
        //  - The player completes one or more lines. In this case the score is exponentially increased by the number of lines completed
        void increaseScore(int nCompleteLines) {_score += _scoreIncrease * pow(2, nCompleteLines);}

        //Getters
        const std::string& getPlayerName() const { return _playerName; }
        bool isAlive() const { return _isAlive; }
        int getScore() const {return _score;}
    private:
        std::string _playerName;
        bool _isAlive;
        int _score;
        int _scoreIncrease;
    };
}

#endif //CONSOLETETRIS_PLAYER_H
