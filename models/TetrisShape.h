//
// Created by convez on 04/04/2020.
//

#ifndef CONSOLETETRIS_TETRISSHAPE_H
#define CONSOLETETRIS_TETRISSHAPE_H

#include <string>
#include <functional>
#include <memory>
#include "../view/GameFieldWindowHandler.h"

namespace convez::games::consoleTetris
{
class TetrisShape {
    public:
        TetrisShape(std::string iShapeModel, int iShapeHeight, int iShapeWidth, char iDrawingSymbol, int iDrawingColor, std::pair<int, int> iStartPosition);

        typedef std::vector<std::function<int(int,int,int)>> IndexFunctionVector_t;
        static IndexFunctionVector_t _rotationIndexCalculatorsVec;
        // Function that changes the current rotation of the shape by 90 degrees, if possible
        bool tryRotate();

        bool tryMoveDown();
        bool tryMoveLeft();
        bool tryMoveRight();

        bool canSpawn() {return _windowHandler->doesPieceFit(*this, _currentPosition);}

        void setWindowHandler(const std::shared_ptr<GameFieldWindowHandler>& iWindowHandler) {_windowHandler = iWindowHandler;}

        // Calculates the index of the element input position, considering current rotation
        char elementAt(int iXPos, int iYPos) const;
        int getShapeHeight() const {return _shapeHeight;}
        int getShapeWidth() const {return _shapeWidth;}
        int getDrawingSymbol() const {return _drawingSymbol | _drawingColor;};
        const std::pair<int,int>& getCurrentPosition() const {return _currentPosition;}
    private:
        // Tries to move the piece to the desired position. Returns true in case of success, false otherwise
        bool tryMoveTo(std::pair<int,int> iPotentialNewPosition);

        void rotate() { _shapeRotation = (_shapeRotation + 1 ) %4; }

        int _shapeHeight;
        int _shapeWidth;
        char _drawingSymbol;
        int _drawingColor;
        int _shapeRotation;
        std::string _shapeModel;
        std::pair<int, int> _currentPosition;
        std::shared_ptr<GameFieldWindowHandler> _windowHandler;
};
}


#endif //CONSOLETETRIS_TETRISSHAPE_H
