#include <iostream>
#include <curses.h>
#include <sstream>
#include <vector>
#include <unistd.h>
#include <random>
#include <chrono>
#include <map>
#include "models/Player.h"
#include "models/TetrisShape.h"
#include "view/ScoreWindowHandler.h"

static const int k_WindowsHeightLines = 20;
static const int k_WindowsWidthLines = 18;
static const int k_ScoreIncrease = 35;
static const int k_NewShapeStartX = k_WindowsWidthLines / 2;
static const int k_NewShapeStartY = 1;

static const int k_FramePerSecond = 60;
static const int k_InitalGameSpeed = 2;
static const int k_IncreaseSpeeadEachNumOfLines = 5;

WINDOW* initScreen();
void    finalizeScreen(WINDOW* aGameWindow);
int detectCompletedLines(char* iGameScreen, int iRowWidth, int iNumRows);
void deleteCompletedRows(char* iGameScreen, int iRowWidth, int iNumRows);

static const std::vector<convez::games::consoleTetris::TetrisShape> aShapeVector
{
    convez::games::consoleTetris::TetrisShape("..X.""..X.""..X.""..X.", 4, 4, 'A', COLOR_PAIR(COLOR_BLUE),std::make_pair(k_NewShapeStartX, k_NewShapeStartY)),
    convez::games::consoleTetris::TetrisShape("..X."".XX."".X..""....", 4, 4, 'B', COLOR_PAIR(COLOR_RED),std::make_pair(k_NewShapeStartX, k_NewShapeStartY)),
    convez::games::consoleTetris::TetrisShape(".X.."".XX.""..X.""....", 4, 4, 'C', COLOR_PAIR(COLOR_GREEN),std::make_pair(k_NewShapeStartX, k_NewShapeStartY)),
    convez::games::consoleTetris::TetrisShape("..X.""..X."".XX.""....", 4, 4, 'D', COLOR_PAIR(COLOR_YELLOW),std::make_pair(k_NewShapeStartX, k_NewShapeStartY)),
    convez::games::consoleTetris::TetrisShape(".X.."".X.."".XX.""....", 4, 4, 'E', COLOR_PAIR(COLOR_MAGENTA),std::make_pair(k_NewShapeStartX, k_NewShapeStartY)),
    convez::games::consoleTetris::TetrisShape(".XX."".XX.""....""....", 4, 4, 'F', COLOR_PAIR(COLOR_CYAN),std::make_pair(k_NewShapeStartX, k_NewShapeStartY)),
    convez::games::consoleTetris::TetrisShape(".X.."".XX."".X..""....", 4, 4, 'G', COLOR_PAIR(COLOR_WHITE),std::make_pair(k_NewShapeStartX, k_NewShapeStartY))
};

int main()
{
    using convez::games::consoleTetris::Player;
    using convez::games::consoleTetris::ScoreWindowHandler;
    using convez::games::consoleTetris::GameFieldWindowHandler;
    using convez::games::consoleTetris::TetrisShape;
    // Initialization
    Player aPlayer(k_ScoreIncrease, "Player 1");

    initscr();
    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_BLUE, COLOR_WHITE);
    {
        ScoreWindowHandler aScoreWindow(3, 35, 0, 0, aPlayer);
        auto aGameWindowHandler = std::make_shared<GameFieldWindowHandler>(k_WindowsHeightLines, k_WindowsWidthLines, 4, 4);

        // Initialize RNG
        const auto aNow = std::chrono::system_clock::now();
        std::default_random_engine aRngEngine(
                std::chrono::duration_cast<std::chrono::milliseconds>(aNow.time_since_epoch()).count());
        std::uniform_int_distribution<int> aRange(0, aShapeVector.size() - 1);
        auto aShapeRng = std::bind(aRange, aRngEngine);

        TetrisShape aTetrisShape = aShapeVector.at(aShapeRng());
        aTetrisShape.setWindowHandler(aGameWindowHandler);
        int aGameSpeed = k_InitalGameSpeed;
        int aFrameCount = 0;

        int aLinesCount = 0;
        //GAME LOOP
        while (aPlayer.isAlive()) {
            // GET INPUT
            usleep(1000000 / k_FramePerSecond); // Game tick
            aFrameCount += aGameSpeed;

            // PROCESS INPUT
            int aPressedKey = aGameWindowHandler->getKeypress();
            switch (aPressedKey) {
                case 'S':
                case 's':
                case KEY_DOWN:
                    aTetrisShape.tryMoveDown();
                    break;
                case 'A':
                case 'a':
                case KEY_LEFT:
                    aTetrisShape.tryMoveLeft();
                    break;
                case 'D':
                case 'd':
                case KEY_RIGHT:
                    aTetrisShape.tryMoveRight();
                    break;
                case 'p':
                    aPlayer.die();  // TODO: Let's temporarily have a way to suicide
                    break;
                case 'W':
                case 'w':
                case KEY_UP:
                    aTetrisShape.tryRotate();
                    break;
                default:
                    break;
            };

            if (aFrameCount == k_FramePerSecond) {
                bool aCanMoveDown = aTetrisShape.tryMoveDown();
                if (!aCanMoveDown) {
                    // Lock in place
                    aGameWindowHandler->lockInPlace(aTetrisShape);
                    // Detect completed lines
                    int aNumCompletedLines = aGameWindowHandler->detectCompletedLines();
                    aPlayer.increaseScore(aNumCompletedLines);
                    if (aNumCompletedLines > 0) {
                        // Reward player with visual queue
                        aScoreWindow.displayScore();

                        // Add current piece to the window
                        usleep(1000000);
                        aGameWindowHandler->deleteCompletedLines();
                        aLinesCount += aNumCompletedLines;
                        if (aLinesCount >= k_IncreaseSpeeadEachNumOfLines) {
                            aLinesCount = aLinesCount % k_IncreaseSpeeadEachNumOfLines;
                            aGameSpeed += 2;
                        }
                    }
                    // Take new shape
                    aTetrisShape = aShapeVector.at(aShapeRng());
                    aTetrisShape.setWindowHandler(aGameWindowHandler);
                    // Try to put it in place
                    if (!aTetrisShape.canSpawn())
                        aPlayer.die();
                }
                aFrameCount = 0;
            }

            // DRAW
            aScoreWindow.displayScore();
            aGameWindowHandler->draw(aTetrisShape);
        }
        aScoreWindow.displayScore();
        aScoreWindow.getKeypress();
    }

    //Finalize
    endwin();
    return 0;
}

WINDOW* initScreen()
{
    initscr();
    WINDOW* aGameWindow = newwin(k_WindowsHeightLines, k_WindowsWidthLines, 4, 4);
    noecho();
    nodelay(stdscr, true);
    cbreak();
    scrollok(aGameWindow, true);
    keypad(stdscr, true);
    return aGameWindow;
}
void finalizeScreen(WINDOW* aGameWindow)
{
    wgetch(aGameWindow);
    werase(aGameWindow);
    endwin();
}

int detectCompletedLines(char* iGameScreen, int iRowWidth, int iNumRows)
{
    int aNumCompletedRows = 0;
    // Loop from second to last Row up to the top
    for(int aRow = iNumRows -2; aRow >=0; --aRow)
    {
        bool isRawCompleted = true;
        // Check only the inner part of the screen
        for(int aCol = 1; aCol < iRowWidth -1 && isRawCompleted; ++aCol)
        {
            if(iGameScreen[aRow * iRowWidth + aCol] == ' ')
            {
                isRawCompleted = false;
            }
        }
        if(isRawCompleted)
        {
            aNumCompletedRows++;
            // Give Player visual queue (fill row with $)
            for(int aCol = 1; aCol < iRowWidth -1; ++aCol)
            {
                iGameScreen[aRow * iRowWidth + aCol] = '$';
            }
        }
    }
    return aNumCompletedRows;
}

void deleteCompletedRows(char* iGameScreen, int iRowWidth, int iNumRows)
{
    // Loop from second to last Row up to the top
    for(int aRow = iNumRows -2; aRow >=0; --aRow)
    {
        // Just check if the first character is a %
        if(iGameScreen[aRow * iRowWidth + 1] == '$')
        {
            // Bring down all the rows above
            for(int aOldRow = aRow; aOldRow > 0; --aOldRow)
            {
                for(int aCol = 1; aCol < iRowWidth -1; ++aCol)
                {
                    iGameScreen[aOldRow * iRowWidth + aCol] = iGameScreen[(aOldRow-1) * iRowWidth + aCol];
                }
            }
            // Clear the first row
            for(int aCol = 1; aCol < iRowWidth -1; ++aCol)
            {
                iGameScreen[aCol] = ' ';
            }
            // Check again this row
            aRow++;
        }
    }
}