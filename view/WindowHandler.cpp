//
// Created by convez on 04/04/2020.
//

#include "WindowHandler.h"

namespace convez::games::consoleTetris
{

    WindowHandler::WindowHandler(int iWindowHeight, int iWindowWidth, int iAbsoluteStartX, int iAbsoluteStartY):
        _windowHeight(iWindowHeight),
        _windowWidth(iWindowWidth)
    {
        _window = newwin(iWindowHeight, iWindowWidth, iAbsoluteStartY, iAbsoluteStartX);

        noecho();
        nodelay(_window, true);
        cbreak();
        scrollok(_window, true);
        keypad(_window, true);
    }

    WindowHandler::~WindowHandler() {
        werase(_window);
    }

    void WindowHandler::draw(const char *aDrawing, int aRelativeX, int aRelativeY)
    {
        wmove(_window, aRelativeY, aRelativeX);
        while(*aDrawing != '\0')
        {
            waddch(_window, *aDrawing);
            aDrawing++;
        }
//        waddstr(_window, aDrawing);
        wrefresh(_window);
    }

    int WindowHandler::getKeypress() {
        return wgetch(_window);
    }
}