//
// Created by convez on 04/04/2020.
//

#include <sstream>
#include "ScoreWindowHandler.h"
namespace convez::games::consoleTetris{

ScoreWindowHandler::ScoreWindowHandler(int iWindowHeight, int iWindowWidth,
                                       int iAbsoluteStartX, int iAbsoluteStartY, const Player& iPlayer)
    : WindowHandler(iWindowHeight, iWindowWidth, iAbsoluteStartX, iAbsoluteStartY),
    _player(iPlayer)
{
    nodelay(_window, false);
    wattron(_window, COLOR_PAIR(2));
    _scoreMessage = _player.getPlayerName() + " is scoring: ";
    _loseMessage = "Sorry, " + _player.getPlayerName()+ "! Try again!";
}

void ScoreWindowHandler::displayScore()
{
    if(!_player.isAlive())
    {
        draw(_loseMessage.c_str(), 0, 0);
    }
    std::stringstream aSs;
    aSs << _scoreMessage << _player.getScore();
    draw(aSs.str().c_str(), 0, 1);
}

}