//
// Created by convez on 04/04/2020.
//

#ifndef CONSOLETETRIS_WINDOWHANDLER_H
#define CONSOLETETRIS_WINDOWHANDLER_H

#include <curses.h>

namespace convez::games::consoleTetris
{
class WindowHandler {
public:
    WindowHandler(int iWindowHeight, int iWindowWidth, int iAbsoluteStartX, int iAbsoluteStartY);

    virtual void draw(const char* aDrawing, int aRelativeX, int aRelativeY);

    ~WindowHandler();

    int getKeypress();

protected:
    const int _windowHeight;
    const int _windowWidth;

    WINDOW* _window;
};
}


#endif //CONSOLETETRIS_WINDOWHANDLER_H
