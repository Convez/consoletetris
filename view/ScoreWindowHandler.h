//
// Created by convez on 04/04/2020.
//

#ifndef CONSOLETETRIS_SCOREWINDOWHANDLER_H
#define CONSOLETETRIS_SCOREWINDOWHANDLER_H

#include <string>
#include "WindowHandler.h"
#include "../models/Player.h"

namespace convez::games::consoleTetris
{
class ScoreWindowHandler : public WindowHandler {
public:
    ScoreWindowHandler(int iWindowHeight, int iWindowWidth, int iAbsoluteStartX, int iAbsoluteStartY, const Player& iPlayer);
    void displayScore();
public:
    std::string _scoreMessage;
    std::string _loseMessage;
    const Player& _player;
};
}


#endif //CONSOLETETRIS_SCOREWINDOWHANDLER_H
