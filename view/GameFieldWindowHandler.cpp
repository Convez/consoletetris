//
// Created by convez on 04/04/2020.
//

#include "GameFieldWindowHandler.h"
#include "../models/TetrisShape.h"

namespace convez::games::consoleTetris
{

GameFieldWindowHandler::GameFieldWindowHandler(int iWindowHeight, int iWindowWidth, int iAbsoluteStartX,
                                               int iAbsoluteStartY) :
    WindowHandler(iWindowHeight, iWindowWidth, iAbsoluteStartX, iAbsoluteStartY),
    _playingField(iWindowHeight*iWindowWidth+1)
{
    for(int i = 0; i < iWindowHeight; ++i)
        for(int j = 0; j < iWindowWidth; ++j)
            _playingField[iWindowWidth * i + j] = (i == (iWindowHeight - 1) || j == 0 || j == (iWindowWidth - 1)) ? '#' : ' ';
    _playingField[iWindowWidth * iWindowHeight]='\0';
}

GameFieldWindowHandler::~GameFieldWindowHandler() {
}

int GameFieldWindowHandler::detectCompletedLines() {
    int aNumCompletedRows = 0;
    // Loop from second to last Row up to the top
    for(int aRow = _windowHeight -2; aRow >=0; --aRow)
    {
        bool isRawCompleted = true;
        // Check only the inner part of the screen
        for(int aCol = 1; aCol < _windowWidth -1 && isRawCompleted; ++aCol)
        {
            if(_playingField[aRow * _windowWidth + aCol] == ' ')
            {
                isRawCompleted = false;
            }
        }
        if(isRawCompleted)
        {
            aNumCompletedRows++;
            // Give Player visual queue (fill row with $)
            for(int aCol = 1; aCol < _windowWidth -1; ++aCol)
            {
                _playingField[aRow * _windowWidth + aCol] = '$';
            }
        }
    }
    WindowHandler::draw(_playingField.data(), 0, 0);
    return aNumCompletedRows;
}

void GameFieldWindowHandler::deleteCompletedLines() {
    // Loop from second to last Row up to the top
    for(int aRow = _windowHeight -2; aRow >=0; --aRow)
    {
        // Just check if the first character is a %
        if(_playingField[aRow * _windowWidth + 1] == '$')
        {
            // Bring down all the rows above
            for(int aOldRow = aRow; aOldRow > 0; --aOldRow)
            {
                for(int aCol = 1; aCol < _windowWidth -1; ++aCol)
                {
                    _playingField[aOldRow * _windowWidth + aCol] = _playingField[(aOldRow-1) * _windowWidth + aCol];
                }
            }
            // Clear the first row
            for(int aCol = 1; aCol < _windowWidth -1; ++aCol)
            {
                _playingField[aCol] = ' ';
            }
            // Check again this row
            aRow++;
        }
    }
    WindowHandler::draw(_playingField.data(), 0, 0);
}
void GameFieldWindowHandler::draw(const TetrisShape &iShape) {
    std::vector aDrawFrame(_playingField.begin(),_playingField.end());
    for(int aShapeY = 0; aShapeY < iShape.getShapeHeight(); ++aShapeY)
    {
        for(int aShapeX = 0; aShapeX < iShape.getShapeWidth(); ++aShapeX)
        {
            char aElementToDraw = iShape.elementAt(aShapeX, aShapeY);
            if(aElementToDraw != '.')
            {
                const auto& aPiecePosition = iShape.getCurrentPosition();
                int aProjectedIndex = TetrisShape::_rotationIndexCalculatorsVec.at(0)(aShapeX+aPiecePosition.first,
                                                                                      aShapeY+aPiecePosition.second,
                                                                                      _windowWidth);
                aDrawFrame[aProjectedIndex] = iShape.getDrawingSymbol();
            }
        }
    }
    WindowHandler::draw(aDrawFrame.data(), 0, 0);
}

bool GameFieldWindowHandler::doesPieceFit(const TetrisShape &iShape, const std::pair<int, int> iPosition) {
    for(int aShapeY = 0; aShapeY < iShape.getShapeHeight(); ++aShapeY)
        for(int aShapeX = 0; aShapeX < iShape.getShapeWidth(); ++aShapeX){
            char aElementToDraw = iShape.elementAt(aShapeX, aShapeY);
            if(aElementToDraw != '.')
                if(_playingField[TetrisShape::_rotationIndexCalculatorsVec.at(0)(aShapeX+iPosition.first,
                                                                                 aShapeY+iPosition.second, _windowWidth)] != ' ')
                    return false;
        }
    return true;
}

void GameFieldWindowHandler::lockInPlace(const TetrisShape &iShape) {
    for(int aShapeY = 0; aShapeY < iShape.getShapeHeight(); ++aShapeY)
    {
        for(int aShapeX = 0; aShapeX < iShape.getShapeWidth(); ++aShapeX)
        {
            char aElementToDraw = iShape.elementAt(aShapeX, aShapeY);
            if(aElementToDraw != '.')
            {
                const auto& aPiecePosition = iShape.getCurrentPosition();
                int aProjectedIndex = TetrisShape::_rotationIndexCalculatorsVec.at(0)(aShapeX+aPiecePosition.first,
                                                                                      aShapeY+aPiecePosition.second,
                                                                                      _windowWidth);
                _playingField[aProjectedIndex] = iShape.getDrawingSymbol();
            }
        }
    }
    WindowHandler::draw(_playingField.data(), 0, 0);
}
}