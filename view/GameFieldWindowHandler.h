//
// Created by convez on 04/04/2020.
//

#ifndef CONSOLETETRIS_GAMEFIELDWINDOWHANDLER_H
#define CONSOLETETRIS_GAMEFIELDWINDOWHANDLER_H

#include <bits/unique_ptr.h>
#include <vector>
#include "WindowHandler.h"

namespace convez::games::consoleTetris
{
class TetrisShape;
class GameFieldWindowHandler: public WindowHandler {
public:
    GameFieldWindowHandler(int iWindowHeight, int iWindowWidth, int iAbsoluteStartX, int iAbsoluteStartY);
    GameFieldWindowHandler(const GameFieldWindowHandler&) = delete;
    GameFieldWindowHandler(GameFieldWindowHandler&&) = delete;
    GameFieldWindowHandler& operator=(const GameFieldWindowHandler&) = delete;
    GameFieldWindowHandler& operator=(GameFieldWindowHandler&&) = delete;
    virtual ~GameFieldWindowHandler();

    int detectCompletedLines();
    void deleteCompletedLines();

    // This function draws a tetris shape to the screen vector and displays it
    // No integrity check is done at this stage. Assuming it was already done at an earlier stage
    void draw(const TetrisShape& iShape);

    // Detects if there is going to be a collision in case we move the piece in the desired new position
    // Returns true in case of no collision, false otherwise
    bool doesPieceFit(const TetrisShape& iShape, const std::pair<int,int> iPosition);

    void lockInPlace(const TetrisShape& iShape);

    std::vector<char> getFrame(){return std::vector<char>(_playingField.begin(), _playingField.end());}
private:
    std::vector<char> _playingField;
};
}


#endif //CONSOLETETRIS_GAMEFIELDWINDOWHANDLER_H
